package br.com.inter.as;

import static br.com.inter.comum.ValidacaoUtils.throwIf;

import java.util.Objects;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.inter.dto.DigitoUnicoParamDTO;
import br.com.inter.service.DigitoUnicoService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DigitoUnicoAS {

	@NonNull
	private DigitoUnicoService service;

	@Transactional
	public Integer calcular(DigitoUnicoParamDTO digitoUnicoParam) {
		throwIf(digitoUnicoParam, Objects::isNull, "Parametros não podem ser nulos.");
		throwIf(digitoUnicoParam.getDigito(), Objects::isNull, "Digito não pode ser nulo.");
		Integer digitoUnico = null;
		if (digitoUnicoParam.getQtdConcatenacao() == null) {
			digitoUnico = this.service.calcular(digitoUnicoParam.getIdUsuario(), digitoUnicoParam.getDigito());
		} else {
			digitoUnico = this.service.calcular(digitoUnicoParam.getIdUsuario(), digitoUnicoParam.getDigito().toString(), digitoUnicoParam.getQtdConcatenacao());
		}
		return digitoUnico;
	}
}