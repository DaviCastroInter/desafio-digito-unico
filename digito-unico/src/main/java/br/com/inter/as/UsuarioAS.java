package br.com.inter.as;

import static br.com.inter.comum.ValidacaoUtils.throwIf;

import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.inter.comum.ChavesCriptografia;
import br.com.inter.comum.CriptografiaUtils;
import br.com.inter.domain.DigitoUnico;
import br.com.inter.domain.Usuario;
import br.com.inter.dto.DigitoUnicoDTO;
import br.com.inter.dto.UsuarioDTO;
import br.com.inter.service.UsuarioService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UsuarioAS {

	@NonNull
	private UsuarioService service;
	@NonNull
	private ConverterMapper mapper;

	@Transactional(readOnly = true)
	public List<UsuarioDTO> findAll() {
		List<Usuario> lstUsuario = this.service.findAll();
		return this.mapper.mapAsList(lstUsuario, UsuarioDTO.class);
	}

	@Transactional(readOnly = true)
	public UsuarioDTO findById(Integer id) {
		Usuario usuario = this.service.findById(id);
		return this.mapper.map(usuario, UsuarioDTO.class);
	}

	@Transactional
	public void deleteById(Integer id) {
		this.service.deleteById(id);
	}

	@Transactional
	public UsuarioDTO save(UsuarioDTO usuarioDTO) {
		Usuario usuario = this.mapper.map(usuarioDTO, Usuario.class);
		usuario = this.service.save(usuario);
		return this.mapper.map(usuario, UsuarioDTO.class);
	}

	@Transactional
	public UsuarioDTO update(Integer id, UsuarioDTO usuarioDTO) {
		throwIf(id, Objects::isNull, "ID usuário não pode ser nulo.");
		boolean existUsuario = this.service.existUsuario(id);
		throwIf(existUsuario, Boolean.FALSE::equals, "Usuário não existe.");
		usuarioDTO.setId(id);
		return this.save(usuarioDTO);
	}

	@Transactional(readOnly = true)
	public List<DigitoUnicoDTO> findAllDigitoUnico(Integer id) {
		List<DigitoUnico> lstDigitoUnico = this.service.findAllDigitoUnico(id);
		return this.mapper.mapAsList(lstDigitoUnico, DigitoUnicoDTO.class);
	}

	public ChavesCriptografia gerarChaves() {
		return CriptografiaUtils.gerarChaves();
	}

	@Transactional
	public void criptografar(Integer id, String chavePublica, String chavePrivada) {
		this.service.criptografar(id, chavePublica, chavePrivada);
	}

	@Transactional(readOnly = true)
	public UsuarioDTO descriptografar(Integer id, String chavePrivada) {
		Usuario usuario = this.service.descriptografar(id, chavePrivada);
		return this.mapper.map(usuario, UsuarioDTO.class);
	}
}