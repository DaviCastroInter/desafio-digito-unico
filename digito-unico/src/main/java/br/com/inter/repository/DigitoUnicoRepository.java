package br.com.inter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.inter.domain.DigitoUnico;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Integer> {

	@Query("SELECT CASE WHEN COUNT(du) > 0 THEN true ELSE false END FROM DigitoUnico du INNER JOIN du.usuario u WHERE u.id = :idUsuario AND du.digito = :digito AND (:qtdConcatenacao IS NULL OR du.qtdConcatenacao = :qtdConcatenacao)")
	public boolean existDigitoUnico(@Param("idUsuario") Integer idUsuario, @Param("digito") String digito, @Param("qtdConcatenacao") Integer qtdConcatenacao);

	@Query("SELECT DISTINCT du FROM DigitoUnico du WHERE du.usuario.id = :idUsuario")
	public List<DigitoUnico> findAllByIdUsuario(@Param("idUsuario") Integer idUsuario);
}