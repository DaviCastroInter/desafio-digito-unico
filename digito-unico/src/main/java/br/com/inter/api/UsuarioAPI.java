package br.com.inter.api;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.as.UsuarioAS;
import br.com.inter.comum.ChavesCriptografia;
import br.com.inter.dto.DigitoUnicoDTO;
import br.com.inter.dto.UsuarioDTO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/usuario")
public class UsuarioAPI {

	@NonNull
	private UsuarioAS as;

	@GetMapping
	public List<UsuarioDTO> findAll() {
		return this.as.findAll();
	}

	@GetMapping("/{id}")
	public UsuarioDTO findById(@PathVariable("id") Integer id) {
		return this.as.findById(id);
	}

	@DeleteMapping
	public void deleteById(@RequestBody Integer id) {
		this.as.deleteById(id);
	}

	@PostMapping
	public UsuarioDTO save(@RequestBody UsuarioDTO usuarioDTO) {
		return this.as.save(usuarioDTO);
	}

	@PutMapping("/{id}")
	public UsuarioDTO update(@PathVariable("id") Integer id, @RequestBody UsuarioDTO usuarioDTO) {
		return this.as.update(id, usuarioDTO);
	}

	@GetMapping("/{id}/find-all-digito-unico")
	public List<DigitoUnicoDTO> findAllDigitoUnico(@PathVariable("id") Integer id) {
		return this.as.findAllDigitoUnico(id);
	}

	@GetMapping("/gerar-chaves-criptografia")
	public ChavesCriptografia gerarChaves() {
		return this.as.gerarChaves();
	}

	@PostMapping("/{id}/criptografar")
	public void criptografar(@PathVariable("id") Integer id, @RequestBody ChavesCriptografia chavesCriptografia) {
		this.as.criptografar(id, chavesCriptografia.getChavePublica(), chavesCriptografia.getChavePrivada());
	}

	@PostMapping("/{id}/descriptografar")
	public UsuarioDTO descriptografar(@PathVariable("id") Integer id, @RequestBody ChavesCriptografia chavesCriptografia) {
		return this.as.descriptografar(id, chavesCriptografia.getChavePrivada());
	}
}