package br.com.inter.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class DigitoUnico extends BaseEntity<Integer> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "id_digito_unico", nullable = false)
	private Integer id;

	@Column(name = "digito", nullable = false)
	private String digito;

	@Column(name = "qtd_concatenacao", nullable = true)
	private Integer qtdConcatenacao;

	@Column(name = "digito_unico", nullable = false)
	private Integer digitoUnico;

	@JoinColumn(name = "id_usuario")
	@ManyToOne(fetch = FetchType.LAZY)
	private Usuario usuario;
}