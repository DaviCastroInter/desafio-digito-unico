package br.com.inter.service;

import static br.com.inter.comum.CacheDigitoUnicoUtils.adicionarCache;
import static br.com.inter.comum.CacheDigitoUnicoUtils.getDigitoUnicoCache;
import static br.com.inter.comum.DigitoUnicoUtils.calcularDigitoUnico;
import static br.com.inter.comum.ValidacaoUtils.throwIf;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.inter.domain.DigitoUnico;
import br.com.inter.domain.Usuario;
import br.com.inter.repository.DigitoUnicoRepository;
import br.com.inter.repository.UsuarioRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class DigitoUnicoService {

	@NonNull
	private DigitoUnicoRepository repository;
	@NonNull
	private UsuarioRepository usuarioRepository;

	@Transactional
	public Integer calcular(Integer idUsuario, Integer digito) {
		throwIf(digito, Objects::isNull, "Digito não pode ser nulo.");
		return this.calcular(idUsuario, digito.toString(), null);
	}

	@Transactional
	public Integer calcular(Integer idUsuario, String digito, Integer qtdConcatenacao) {
		throwIf(digito, StringUtils::isEmpty, "Digito não pode ser nulo.");
		Integer digitoUnico = this.definirDigitoUnico(digito, qtdConcatenacao);
		this.salvarDigitoUnicoUsuario(idUsuario, digito, qtdConcatenacao, digitoUnico);
		return digitoUnico;
	}

	private Integer definirDigitoUnico(String digito, Integer qtdConcatenacao) {
		Integer digitoUnico = getDigitoUnicoCache(digito, qtdConcatenacao);
		if (digitoUnico == null) {
			digitoUnico = (qtdConcatenacao == null) ? calcularDigitoUnico(Integer.valueOf(digito)) : calcularDigitoUnico(digito, qtdConcatenacao);
			adicionarCache(digito, qtdConcatenacao, digitoUnico);
		}
		return digitoUnico;
	}

	private void salvarDigitoUnicoUsuario(Integer idUsuario, String digito, Integer qtdConcatenacao, Integer digitoUnico) {
		if (idUsuario != null && this.usuarioRepository.existUsuario(idUsuario)) {
			boolean existDigitoUnicoUsuario = this.repository.existDigitoUnico(idUsuario, digito, qtdConcatenacao);
			if (!existDigitoUnicoUsuario) {
				Usuario usuario = Usuario.builder().id(idUsuario).build();
				DigitoUnico entity = DigitoUnico.builder().digito(digito).qtdConcatenacao(qtdConcatenacao).digitoUnico(digitoUnico).usuario(usuario).build();
				this.repository.save(entity);
			}
		}
	}
}